#include "constans.h"
#include "operation.h"
#include "File_Reader.h"
#include "output.h"
#include "filter.h"
#include "process.h"
#include <iostream>

using namespace std;

int main()
{
    setlocale(LC_ALL, "rus");
    cout << "Laboratory work #8. GIT\n";
    cout << "Variant #7. Bank\n";
    cout << "Author: Sobolevskaya Liza\n";
    cout << "Group: 16\n";


    opers* array[MAX_FILE_ROWS_COUNT];
    int size;

    try {

        read("data.txt", array, size);

        for (int i = 0; i < size; i++) {
            output(array[i]);
        }

        int k;
        cout << "�������� ��� ������: ";
        cin >> k;
        switch (k) {
        case 1: typefilt(array, size);
            break;
        case 2: monthfilt(array, size);
            break;
        case 3: count(array, size);
        }

        for (int i = 0; i < size; i++) {
            delete array[i];
        }
    }
    catch (const char* error) {
        cout << error << '\n';
    }
    return 0;


}