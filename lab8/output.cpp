#include "output.h"
#include <cstring>
#include <iostream>
#include <string>

using namespace std;

void output(opers* array) {

	cout << array->date.day << ".";
	cout << array->date.month << ".";
	cout << array->date.year << " ";
	cout << array->time.hour << ".";
	cout << array->time.min << ".";
	cout << array->time.sec << " ";
	cout << array->type << " ";
	cout << array->account << " ";
	cout << array->value << " ";
	cout << array->purpose << " ";

	cout << '\n';

}