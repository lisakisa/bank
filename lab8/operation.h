#include "constans.h"

#ifndef OPERATION_H
#define OPERATION_H

struct date {
    int day;
    int month;
    int year;
};

struct times {
    int sec;
    int min;
    int hour;
};

struct opers {
    date date;
    times time;
    double value;
    char type[MAX_STRING_SIZE];
    char account[MAX_STRING_SIZE];
    char purpose[MAX_STRING_SIZE];
};
#endif